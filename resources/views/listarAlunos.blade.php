
    <html>
        <body>
            <h1>LISTAR ALUNOS</h1>

            @error('erro')
            <div style= "color: red">
                {{$message}}
            </div>
            @enderror
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($alunos as $aluno)
                        <tr>
                            <td> {{ $aluno->id}} </td>
                            <td> {{$aluno->nome}} </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </body>
    </html>

